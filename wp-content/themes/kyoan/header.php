<!DOCTYPE html>
<html lang="ja">
<head>
<title>京安工業株式会社</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<?php wp_head();?>
</head>
<body <?php body_class();?>>
	<div id="mask"></div>
	<header>
		<h1><a href="<?php echo home_url();?>"><img class="pcOnly" src="<?php echo get_stylesheet_directory_uri(); ?>/img/tmp_header_logo.png" alt="京安工業株式会社"><img class="spOnly" src="<?php echo get_stylesheet_directory_uri(); ?>/img/tmp_header_logo_sp.png" alt="京安工業株式会社"></a></h1>

		<div id="navBtn" class="spOnly"><i class="fa fa-bars" aria-hidden="true"></i></div>

		<nav class="mainNav pcOnly">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/sp_nav_close.png" class="close spOnly">
			<ul>
				<li><a href="<?php echo home_url();?>">HOME</a></li>
				<li><a href="<?php echo home_url('product');?>">取扱商品</a></li>
				<li><a href="<?php echo home_url('company');?>">会社案内</a></li>
				<li><a href="<?php echo home_url('history');?>">会社沿革</a></li>
				<li><a href="<?php echo home_url('news');?>">新着情報</a></li>
				<li><a href="<?php echo home_url('contact');?>">お問い合わせ</a></li>
			</ul>
		</nav>
	</header>
