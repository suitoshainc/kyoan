<?php get_header(); ?>
<div class="subpage_head">
	<div class="wrapWidth txtCenter">
		<div class="subpage_title"><h2>取扱商品</h2></div>
	</div>
</div>

<?php
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('
			<p id="breadcrumbs" class="pcOnly breadcrumbs wrapWidth">','</p>
		');
	}
?>

<div class="product_01 wrapWidth txtCenter">
	<div class="product_title">
		<p>生産設備の高速化・<br>高精度化に貢献する<br>モーションコントロール製品</p>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/p_topimg.png" alt="">
	</div>
</div>

<?php
	$html = array();
	$myQuery = new WP_Query();
	parse_str( $query_string,$param);//デフォルトのクエリを引き継ぎたい場合
	$param['posts_per_page'] = -1;
	$myQuery->query($param);
?>
<?php if($myQuery->have_posts()): while($myQuery->have_posts()) :
$myQuery->the_post();
$product_main_img = get_field('product_main_img');
$product_main_img_src = wp_get_attachment_image_src( $product_main_img ,'full' );

$product_logo = get_field('product_logo');
$product_logo_src = wp_get_attachment_image_src( $product_logo, 'full');

$product_maker = get_field('product_maker');
$product_link = get_field('product_link');
$product_desc = get_field('product_desc');
ob_start();
?>
	<div id="p_<?php echo $post->ID;?>" class="p_left">
		<?php if($product_main_img_src):?>
		<img class="p_photo" src="<?php echo $product_main_img_src[0];?>" alt="<?php the_title();?>写真">
		<?php endif;?>
		<div class="logo_txt">
			<h3 class="p_name"><?php the_title();?></h3>

				<?php if($product_logo_src):?>
					<div class="p_ls">
					<img class="p_logo" src="<?php echo $product_logo_src[0];?>" alt="<?php echo $product_maker;?>">
					</div>
				<?php else:?>
					<p style="color:#05519d; font-size:22px; font-weight: bold;"><?php echo $product_maker;?></p>
				<?php endif;?>

				<?php if($product_link):?>
				<a href="<?php echo $product_link;?>" target="_blank"><img class="spOnly p_shosai" src="<?php echo get_stylesheet_directory_uri(); ?>/img/p_shosai_sp.png" alt="詳細を見る"><img class="pcOnly p_shosai" src="<?php echo get_stylesheet_directory_uri(); ?>/img/p_shosai1.png" alt="詳細を見る"> </a>
				<?php endif;?>
		</div>

		<?php if($product_desc ):?>
		<p class="p_txt cf" style="clear:both;"><?php echo $product_desc;?></p>
		<?php endif;?>
	</div>
<?php
$contents = ob_get_clean();
$html[] = $contents;
endwhile;endif;wp_reset_postdata();

$html = array_chunk($html, 2);
?>

<div class="product_02">
	<?php foreach($html as $h):?>
		<div class="wrapWidth txtCenter cf p_wrap">
			<?php
			foreach($h as $t){
				echo $t;
			}
			?>
		</div>
	<?php endforeach;?>
</div>
<?php get_footer(); ?>
