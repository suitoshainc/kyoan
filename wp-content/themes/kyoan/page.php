<?php get_header(); ?>
<div class="subpage_head">
	<div class="wrapWidth txtCenter">
		<div class="subpage_title"><h2><?php the_title();?></h2></div>
	</div>
</div>

<?php
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('
			<p id="breadcrumbs" class="pcOnly breadcrumbs wrapWidth">','</p>
		');
	}
?>
<?php the_content();?>

<?php if(is_page('company')):?>
<div id="map"></div>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/map.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>
<?php endif;?>
<?php get_footer(); ?>
