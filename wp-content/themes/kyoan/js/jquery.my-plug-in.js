jQuery.noConflict();
(function($) {


$(function(){
	//div link
	$(".boxLink").click(function(){
		window.location=$(this).find("a").attr("href");
		return false;
	});


	//accordion
	$(document).ready(function(){
	    $('.acod').click(function() {
	        $(this).next('dd').slideToggle('slow');
    }).next().hide();});


	//accordion
	$(document).ready(function(){
	    $('.spAcod').click(function() {
	        $(this).next('ul').slideToggle('slow');
    }).next().hide();});



	$("#navBtn").click(function(){
		$("#mask").fadeIn("fast");
		$(".mainNav").fadeIn("fast");
	});
	$("#mask").click(function(){
		$("#mask").hide();
		$(".mainNav").hide();
	});
	$("nav .close").click(function(){
		$("#mask").hide();
		$(".mainNav").hide();
	});


	$("#fontSize span").click(function(){
		var fontCss = $(this).attr("class");
		if(fontCss == "small"){
		  $("p").css("fontSize","1.4em");
		}else{
		  $("p").css("fontSize","2em");
		}

 	});

});

})(jQuery);
