<?php get_header(); ?>
<div class="subpage_head">
	<div class="wrapWidth txtCenter">
		<div class="subpage_title"><h2>会社沿革</h2></div>
	</div>
</div>

<?php
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('
			<p id="breadcrumbs" class="pcOnly breadcrumbs wrapWidth">','</p>
		');
	}
?>

<div class="company_03">
	<div class="wrapWidth txtCenter">
		<dl>
			<?php
				if( have_rows('history_repeat') ):
				while ( have_rows('history_repeat') ) : the_row();
			?>
				<dt><?php echo get_sub_field('history_time');?></dt>
				<dd><?php echo get_sub_field('history_content');?></dd>
			<?php endwhile;endif;?>
		</dl>
	</div>
</div>
<?php get_footer(); ?>
