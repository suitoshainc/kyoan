<?php get_header(); ?>
<div class="pcOnly topImg cf txtCenter">
	<h2>「お客様が主人公」をモットーに<br>「感動」をお客様と共有できる技術提案を･･･</h2>
	<p>㈱安川電機殿インバータ製造ライン</p>
</div>
<div class="spOnly topImgsp cf txtCenter">
	<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top_main_sp.png" alt="京安工業株式会社メインイメージ">
	<h2>「お客様が主人公」をモットーに<br>「感動」をお客様と共有できる技術提案を･･･</h2>
</div>

<section class="news wrapWidth txtCenter">

		<h2>新着情報</h2>
		<div class="ichiran spOnly"><a href="<?php echo home_url('news');?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/tmp_ichiran_sp.png" alt="一覧を見る"></a></div>
		<div class="ichiran pcOnly"><a href="<?php echo home_url('news');?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/tmp_ichiran.png" alt="一覧を見る"></a></div>
		<div class="newsdl">
			<?php
				$myQuery = new WP_Query();
				$param = array(
					'posts_per_page' => 5,
					'post_type' => 'news'
				);
				$myQuery->query($param);
			?>
			<?php if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post(); ?>
				<dl>
					<dt><?php the_time('Y | m | d')?></dt>

					<?php
						$link = '';
						if(get_field('news_file')){
							$link = get_field('news_file');
							$target = "_blank";
						}

						if(get_field('news_link')){
							$link = get_field('news_link');
							$target = "_blank";
						}

						if(!$link){
							$link = get_permalink();
							$target = "_top";
						}

						if($link):
					?>
						<dd><a href="<?php echo $link;?>" target="<?php echo $target;?>"><?php the_title();?></a></dd>
					<?php else:?>
						<dd><a href="<?php echo home_url('news');?>/#p_<?php echo $post->ID;?>"><?php the_title();?></a></dd>
					<?php endif;?>

				</dl>
			<?php endwhile;endif;wp_reset_postdata();?>

		</div>

</section>

<section class="shohin txtCenter">
	<h2>取扱商品</h2>
	<div class="ichiran spOnly"><a href="<?php echo home_url('product')?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/tmp_ichiran_sp.png" alt="一覧を見る"></a></div>
	<div class="ichiran pcOnly"><a href="<?php echo home_url('product')?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/tmp_ichiran.png" alt="一覧を見る"></a></div>
	<h3>生産設備の高速化・高精度化に貢献する<br class="spOnly">モーションコントロール製品</h3>

	<?php
		$html = array();
		$myQuery = new WP_Query();
		$param = array(
			'posts_per_page' => -1,
			'post_type' => 'product'
		);
		$myQuery->query($param);
	if($myQuery->have_posts()): while($myQuery->have_posts()) :
	$myQuery->the_post();
	$product_list_img = get_field('product_list_img');
	$product_list_img_src = wp_get_attachment_image_src( $product_list_img ,'full' );
	ob_start();
	?>
		<li><a href="<?php echo home_url('product')?>#p_<?php echo $post->ID;?>"><img class="box_left" src="<?php echo $product_list_img_src[0];?>" alt="<?php the_title();?>"></a></li>
	<?php
	$contents = ob_get_clean();
	$html[] = $contents;
	endwhile;endif;wp_reset_postdata();

	$html2 = array_chunk($html, 2);
	$html4 = array_chunk($html, 4);
	?>


	<?php foreach($html2 as $h2):?>
		<div class="pcOnly shohinbox wrapWidth">
			<ul>
				<?php
				foreach($h2 as $t2){
					echo $t2;
				}
				?>
			</ul>
		</div>
	<?php endforeach;?>

	<?php foreach($html4 as $h4):?>
		<div class="spOnly shohinbox_sp wrapWidth">
			<ul>
				<?php
				foreach($h4 as $t4){
					echo $t4;
				}
				?>
			</ul>
		</div>
	<?php endforeach;?>
</section>
<?php get_footer(); ?>
