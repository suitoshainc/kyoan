<?php get_header(); ?>
<div class="subpage_head">
	<div class="wrapWidth txtCenter">
		<div class="subpage_title"><h2><?php the_title();?></h2></div>
	</div>
</div>

<?php
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('
			<p id="breadcrumbs" class="pcOnly breadcrumbs wrapWidth">','</p>
		');
	}
?>
<div class="company_03">
	<div class="wrapWidth">
	<?php the_content();?>
	</div>
</div>

<?php get_footer(); ?>
