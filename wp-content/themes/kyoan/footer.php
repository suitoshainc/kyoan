<footer>
		<div class="wrapWidth cf">
		<div class="pcOnly">
			<a href="<?php echo home_url();?>"><img class="ftlogo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/tmp_ft_logo.png" alt="京安工業株式会社ロゴマーク"></a>
		</div>
		<div class="spOnly">
			<div class="ftlogosp txtCenter">
				<a href="<?php echo home_url();?>"><img class="" src="<?php echo get_stylesheet_directory_uri(); ?>/img/tmp_ft_logo.png" alt="京安工業株式会社ロゴマーク"></a>
			</div>
		</div>

			<address>
				<div class="ftctr">
					<h6>京安工業株式会社</h6>
					<p>KYOAN KOGYO CO,. LTD.</p>
					<p class="adfirst">〒615-0031 京都市右京区西院高田町3番地</p>
					<p>TEL 075-311-5176　FAX 075-321-5580</p>
					<a href="<?php echo home_url('company')?>#map"><img class="spOnly access" src="<?php echo get_stylesheet_directory_uri(); ?>/img/btn_access_sp.png" alt="アクセス"><img class="pcOnly" src="<?php echo get_stylesheet_directory_uri(); ?>/img/btn_access.png" alt="アクセス"></a>
				</div>
			</address>
		</div>
		<div class="copyright cf">
			<div class="wrapWidth">
				<div class="privacy"><p><a href="<?php echo home_url('policy')?>">プライバシーポリシー</a>　|　<a href="<?php echo home_url('sitemap');?>">サイトマップ</a></p></div>
				<div class="copyleft"><p>Copyright(C) KYOAN KOGYO CO,. LTD. All Right Reserved.</p></div>

			</div>
		</div>
	</footer>
<?php wp_footer();?>
</body>
</html>
