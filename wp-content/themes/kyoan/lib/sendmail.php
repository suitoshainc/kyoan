<?php
//送信先メールアドレス
$MAIL = 'saito@alpha-site.jp';

//フォームのデータ受け取り
foreach($_POST as $key => $val) $$key = (!$val || $val == 'undefined')?'--':$val;



$sub = "公式サイトよりお問い合わせ";
$body = <<<EOM
■会社名
{$com}

■部署名
{$busho}

■お名前
{$name}

■メールアドレス
{$mail}

■住所
{$add}

■電話番号
{$tel}

■FAX番号
{$fax}

■内容
{$inprad}
{$naiyo}

━━━━━━━━━━━━━━━━━━━━━━
送信元:京安工業株式会社
━━━━━━━━━━━━━━━━━━━━━━
EOM;


//送信処理
if($sub && $body){
	require("qdmail.php");
	$mail=new Qdmail();
	$mail->to($MAIL);
	$mail->subject($sub);
	$mail->text($body);
	$mail->from($MAIL);
	$mail->send();
}

header('Content-type:text/plain;charset=utf8');
?>