<?php get_header(); ?>

<div class="subpage_head">
	<div class="wrapWidth txtCenter">
		<div class="subpage_title"><h2>新着情報</h2></div>
	</div>
</div>

<?php
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('
			<p id="breadcrumbs" class="pcOnly breadcrumbs wrapWidth">','</p>
		');
	}
?>

<div class="topics">
	<div class="wrapWidth cf">
		<div class="newsdl">
			<?php
				$myQuery = new WP_Query();
				$param = array(
					'posts_per_page' => 5,
					'post_type' => 'news'
				);
				$myQuery->query($param);
			?>
			<?php if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post(); ?>
				<dl>
					<dt><?php the_time('Y | m | d')?></dt>

					<?php
						$link = '';
						if(get_field('news_file')){
							$link = get_field('news_file');
							$target = "_blank";
						}

						if(get_field('news_link')){
							$link = get_field('news_link');
							$target = "_blank";
						}

						if(!$link){
							$link = get_permalink();
							$target = "_top";
						}


						if($link):
					?>
						<dd><a href="<?php echo $link;?>" target="<?php echo $target;?>"><?php the_title();?></a></dd>
					<?php else:?>
						<dd><?php the_title();?></dd>
					<?php endif;?>

				</dl>
			<?php endwhile;endif;wp_reset_postdata();?>
		</div>
  	</div>
</div>

<?php get_footer(); ?>
